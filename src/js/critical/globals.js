/**
* Globals
*/
AppGlobal = {}
AppSmaller = {}
AppLarger = {}


Globals = ( function() {

    var globals = {

    }

    var init = function() {
        set( 'breakpoint', $( 'title' ).css( 'width' ) );

        var mediaQuery = $( 'title' ).css( 'fontFamily' ) || $( 'title' ).css( 'font-family' );
        set( 'mediaQuery', mediaQuery
            .replace( /'/g, '' )
            .replace( /"/g, '' )
        );
        set( 'transitionDuration', ( parseFloat( $( 'title' ).css( 'transitionDuration' ) ) * 1000 ) || 500 );
        Debug.log( globals );
    }

    var set = function( key, value ) {
        globals[key] = value;
    }

    var get = function( key ) {
        return globals[key];
    }

    return {
        init: function() { init(); },
        set:  function( key, value ) { set( key, value ) },
        get:  function( key ) { return get( key ) }
    }

} )();

Globals.init();
