$( document ).ready( function() {
    yepnope( [
        {
            test: Modernizr.mq( Globals.get( 'mediaQuery' ) ),
            yep : [ Globals.get( 'url' ) + '/js/all-smaller.min.js' ],
            nope: [ Globals.get( 'url' ) + '/js/all-larger.min.js' ]
        }
    ] );
} );
