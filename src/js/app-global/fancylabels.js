/**
* fancyLabels
*/
AppGlobal.FancyLabels = ( function() {

        var settings = {
            selector: {
                'form':  '.form',
                'row':   '.fancylabels',
                'input': '.fancylabels input[type="text"], .fancylabels input[type="email"], .fancylabels textarea'
            },
            element: {}
        };

        var init = function() {
            bindEventHandlers();

            // wait for auto fill
            setTimeout( function() {
                $( settings.selector.input ).each( function() {
                    checkField( $( this ) );

                    $( 'html' )
                        .addClass( 'initiated--fancylabels' );
                } );
            }, 1000 );
        }

        var bindEventHandlers = function() {
            $( document )
                .on( 'feedback/render', function() {
                    $( settings.selector.input ).each( function() {
                        checkField( $( this ) );
                    } );
                } )
                .on( 'focus', settings.selector.input, function() {
                    onFocus( $( this ) )
                } )
                .on( 'blur', settings.selector.input, function() {
                    onBlur( $( this ) )
                } );
        }

        var onFocus = function( field ) {
            field.closest( settings.selector.row )
                .addClass( 'not-empty' );
        }


        var onBlur = function( field ) {
            if( field.val() === '' || !field.val() ) {
                field.closest( settings.selector.row )
                    .removeClass( 'not-empty' );
            } else {
                field.closest( settings.selector.row )
                    .addClass( 'not-empty' );
            }
        }

        var checkField = function( field ) {
            Debug.log( 'AppGlobal.FancyLabels.checkField()', field, field.val() );

            if( field.val() === '' || !field.val() ) {
                field.closest( settings.selector.row )
                    .removeClass( 'not-empty' );
            } else {
                field.closest( settings.selector.row )
                    .addClass( 'not-empty' );
            }
        }

        return {
            init: function() { return init(); }
        }

    } )();

$( document ).ready( function() {
    AppGlobal.FancyLabels.init();
} );
