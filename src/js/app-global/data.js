/**
* Data
*/
AppGlobal.Data = ( function() {
    var settings = {
        api:{
            url: 'lib/Endpoints.php'
        }
    }

    var selector = {
        meta: 'meta[name="location"]'
    }

    var data = {
        'customer': null,
        'location': null,
        'places': false,
        'feedback': false
    }

    var state = {}

    var init = function() {
        Debug.log( 'AppGlobal.Data.init()' );

        if( !$( selector.meta ).length ) {
            return false;
        }

        settings.customerId = parseInt( $( selector.meta ).attr( 'data-customer' ) );
        settings.locationId = parseInt( $( selector.meta ).attr( 'data-location' ) );

        data.places = parseInt( $( selector.meta ).attr( 'data-places' ) );
        data.feedback = parseInt( $( selector.meta ).attr( 'data-feedback' ) );

        Debug.log( $( selector.meta ) );
        Debug.log( 'data', data );

        loadData();
    }

    var bindEventHandlers = function() {

    }

    var loadData = function() {
        Debug.log( 'AppGlobal.Data.loadData()' );

        // customer data
        state.loading = true;
        var url = settings.api.url + '?action=getCustomer&customerId=' + settings.customerId;
        $.getJSON( url, function() {} )
            .done( function( _data ) {
                Debug.log( 'loaded data', data );
                data.customer = _data;

                $( document ).trigger( 'data/load/customer', [{ data: _data }] );
                state.loading = false;

                if( data.location ) {
                    $( document ).trigger( 'data/load/customer+location' );
                }

                Debug.log( 'data.customer', data.customer );
            } )
            .fail( function( jqxhr, textStatus, error ) {
                Debug.log( 'failed to load data', error );

                $( document ).trigger( 'data/load/error' );
            } );

        // location data
        var url = settings.api.url + '?action=getLocation&customerId=' + settings.customerId + '&locationId=' + settings.locationId;
        $.getJSON( url, function() {} )
            .done( function( _data ) {
                Debug.log( 'loaded data', data );

                data.location = _data;

                $( document ).trigger( 'data/load/location', [{ data: _data }] );
                state.loading = false;

                if( data.customer ) {
                    $( document ).trigger( 'data/load/customer+location' );
                }

                Debug.log( 'data.location', data.location );
            } )
            .fail( function( jqxhr, textStatus, error ) {
                Debug.log( 'failed to load data', error );

                $( document ).trigger( 'data/load/error' );
            } );
    }

    var get = function( key ) {
        return data[key];
    }

    return {
        init:   function() { return init(); },
        get:    function( key ) { return get( key ); }
    }
} )();

$( document ).ready( function() {
    AppGlobal.Data.init();
} );
