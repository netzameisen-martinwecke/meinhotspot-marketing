/**
* Title
*/
AppGlobal.Title = ( function() {
    var settings = {}

    var selector = {
        title: '[data-title-role="title"]'
    }

    var state = {

    }

    var data = {}

    var init = function() {
        Debug.log( 'AppGlobal.Title.init()' );

        bindEventHandlers();
    }

    var bindEventHandlers = function() {
        $( document )
            .on( 'data/load/customer+location', function() {
                data.customer = AppGlobal.Data.get( 'customer' );
                data.location = AppGlobal.Data.get( 'location' );
                render();
            } );
    }

    var render = function( _data ) {
        Debug.log( 'AppGlobal.Title.render()', _data );

        $( selector.title ).each( function() {
            var title = $( this );
            var name = data.location.contact_name || data.customer.name || '';

            title
                .text( name )
                .addClass( 'populated' );
        } );
    }


    return {
        init: function() { return init(); }
    }
} )();

$( document ).ready( function() {
    AppGlobal.Title.init();
} );
