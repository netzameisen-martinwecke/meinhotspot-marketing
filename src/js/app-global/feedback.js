/**
* Map
*/
AppGlobal.Feedback = ( function() {
    var settings = {
        api: {
            url: 'lib/Endpoints.php'
        },
        timeout: 8000
    }

    var data = {}

    var selector = {
        widget:     '.widget--feedback',
        content:    '.widget-content--feedback',
        form:       '.feedback-form',
        comment:    '[data-feedback-role="comment-input"]',
        email:      '[data-feedback-role="email-input"]',
        rate:       '[name="feedback-rate"]:checked',
        submit:     '.feedback-submit',
        label:      '.feedback-rate-label'
    }

    var state = {
        loaded: {
            location: false,
            customer: false
        },
        valid: false
    }

    var init = function() {
        Debug.log( 'AppGlobal.Feedback.init()' );

        if( $( 'meta[name="location"]' ).attr( 'data-feedback' ) != '1' ) {
            return false;
        }

        bindEventHandlers();
    }

    var bindEventHandlers = function() {
        $( document )
            .on( 'data/load/customer+location', function() {
                data.location = AppGlobal.Data.get( 'location' );
                data.customer = AppGlobal.Data.get( 'customer' );

                state.loaded.location = true;
                state.loaded.customer = true;

                render();
            } )
            .on( 'data/load/error', function() {
                destroy();
            } )
            .on( 'click', selector.submit, function( event ) {
                if( state.valid ) {
                    onSubmit();
                }
            } )
            .on( 'click', selector.label, function() {
                var label = $( this );
                label
                    .addClass( 'blink' );
                setTimeout( function() {
                    label
                        .removeClass( 'blink' );
                }, 1000 );
            } )
            .on( 'change', selector.rate, function( event ) {
                onChange();
            } )
            .on( 'feedback/submit/success', function() {
                onSuccess();
            } );
    }

    var onSuccess = function() {
        $( selector.widget )
            .addClass( 'success' );

        $( document ).trigger( 'widget/change' )
    }

    var onChange = function() {
        var value = $( selector.rate ).val();

        if( value ) {
            state.valid = true;

            $( selector.form )
                .removeClass( 'invalid' );
        }
    }

    var onSubmit = function() {
        var rate = $( selector.rate ).val();
        var comment = $( selector.comment ).val();
        var email = $( selector.email ).val();

        var _data = {
            action: 'putFeedback',
            rate: rate,
            comment: comment,
            email: email,
            locationId: data.location.id
        }

        Debug.log( _data );

        $( document ).trigger( 'feedback/submit' )

        $.post( settings.api.url, _data )
            .done( function( response ) {
                Debug.log( 'done' );
                $( document ).trigger( 'feedback/submit/success' );
            } )
            .fail( function( response ) {
                Debug.log( 'fail' );
                $( document ).trigger( 'feedback/submit/error' );
            } );
    }

    var render = function( _data ) {
        Debug.log( 'AppGlobal.Feedback.render()', _data );

        $( selector.widget )
            .addClass( 'loading' );

        var template = $( '#template--feedback' ).html();

        Mustache.parse( template );

        var _data = {
            title: data.location.contact_name || data.customer.name,
            address: {
                line1: data.location.street + ' ' + data.location.number,
                line2: data.location.zip + ' ' + data.location.location
            },
            contact: {
                phone: data.location.contact_phone,
                mail: data.location.contact_email
            },
            url: data.location.contact_web
        }

        var rendered = Mustache.render(
            template,
            _data
        );

        $( selector.content )
            .append( $( rendered ) );

        $( selector.form )
            .addClass( 'invalid' );

        $( selector.widget )
            .removeClass( 'loading' )
            .addClass( 'populated' );

        $( document ).trigger( 'feedback/render' );

        autosize( $( selector.comment ) );
    }

    var destroy = function() {
        Debug.log( 'AppGlobal.Feedback.destroy()' );


        $( selector.widget )
            .addClass( 'unloading' );

        setTimeout( function() {
            $( selector.widget ).remove();

            $( document ).trigger( 'widget/change' );
        }, 500 );
    }

    return {
        init: function() { return init(); }
    }
} )();

$( document ).ready( function() {
    AppGlobal.Feedback.init();
} );
