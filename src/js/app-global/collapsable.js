/**
* Collapsable
*/
AppGlobal.Collapsable = ( function() {

        var settings = {
            maxWidth: 0
        };

        var selector = {
            toggle: '[data-collapsable-role="toggle"]',
            widget: '[data-collapsable-role="widget"]'
        };


        var init = function() {
            if( $( window ).width() < settings.minWidth ) {
                return false;
            }

            bindEvents();

            $( selector.widget )
                .addClass( 'collapsed' );

            $( 'html' )
                .addClass( 'initiated--collapsable' );
        }

        var bindEvents = function() {
            $( document )
                .on( 'click', selector.toggle, function( event ) {
                    var widget = $( event.target ).closest( selector.widget );

                    widget
                        .toggleClass( 'collapsed' );
                } );
        }

        return {
            init: function() { return init(); }
        }
    } )();

$( document ).ready( function() {
    AppGlobal.Collapsable.init();
} );
