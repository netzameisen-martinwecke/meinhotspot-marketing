module.exports = function( grunt ) {

    require( 'matchdep' ).filterDev( 'grunt-*' ).forEach( grunt.loadNpmTasks );

    grunt.initConfig( {
        pkg: grunt.file.readJSON( 'package.json' ),

        mustache_render: {
            all: {
                options: {
                    data:       'src/data.json',
                    directory:  'src/partials'
                },
                files: {
                    'dist/index.php': 'src/pages/index.mustache'
                }
            }
        },

        prettify: {
            options: {
                'indent': 4,
                'condense': true,
                'indent_inner_html': true,
            },
            all: {
                expand: true,
                cwd: 'dist/',
                ext: '.html',
                src: ['*.html'],
                dest: ''
            }
        },

        less: {
          development: {
            files: {
              'dist/css/style.css': 'src/less/style.less'
            }
          }
        },

        autoprefixer: {
            style: {
              src:  'dist/css/style.css',
              dest: 'dist/css/style.css'
            }
        },

        modernizr: {
            dist: {
                'dest' : 'src/js/critical/modernizr.min.js',
                'options' : [
                    'setClasses',
                    'addTest',
                    'html5printshiv',
                    'testProp',
                    'fnBind',
                    'mq'
                ],
                'tests' : [
                    'touchevents',
                    'pointerevents'
                ],
                'files' : {
                    'src': [
                        'src/js/**/*.js',
                        'dist/css/**/*.css',
                        '!node_modules/**/*',
                        '!js/**/*.min.js'
                    ]
                }
            }
        },

        uglify: {
            options: {
                mangle: false,
                sourceMap: true,
                sourceMapName: function( filename ) {
                    var parts = filename.split( '/' );
                    return 'src/js/sourcemaps/' + parts[parts.length-1] + '.map'
                }
            },
            critical: {
              files: {
                'dist/js/critical.min.js': [
                    'src/js/critical/modernizr.min.js',
                    'src/js/critical/jquery.min.js',
                    'src/js/critical/debug.js',
                    'src/js/critical/globals.js',
                ]
              }
            },
            dependenciesGlobal: {
              files: {
                'src/js/dependencies-global.min.js': ['src/js/dependencies-global/*.js']
              }
            },
            dependenciesSmaller: {
              files: {
                'src/js/dependencies-smaller.min.js': ['src/js/dependencies-smaller/*.js']
              }
            },
            dependenciesLarger: {
              files: {
                'src/js/dependencies-larger.min.js': ['src/js/dependencies-larger/*.js']
              }
            },
            appGlobal: {
                files: {
                    'src/js/app-global.min.js': [
                        'src/js/app-global/fancylabels.js',
                        'src/js/app-global/collapsable.js',
                        'src/js/app-global/feedback.js',
                        'src/js/app-global/title.js',
                        'src/js/app-global/data.js'
                    ]
                }
            },
            appSmaller: {
                files: {
                    'src/js/app-smaller.min.js': [
                    ]
                }
            },
            appLarger: {
                files: {
                    'src/js/app-larger.min.js': [

                    ]
                }
            },
            allGlobal: {
                files: {
                    'dist/js/all-global.min.js': [
                        'dist/js/critical.min.js',
                        'src/js/dependencies-global.min.js',
                        'src/js/app-global.min.js'
                    ]
                }
            },
            allSmaller: {
                files: {
                    'dist/js/all-smaller.min.js': [
                        'dist/js/critical.min.js',
                        'src/js/dependencies-global.min.js',
                        'src/js/app-global.min.js',
                        'src/js/dependencies-smaller.min.js',
                        'src/js/app-smaller.min.js'
                    ]
                }
            },
            allLarger: {
                files: {
                    'dist/js/all-larger.min.js': [
                        'dist/js/critical.min.js',
                        'src/js/dependencies-global.min.js',
                        'src/js/app-global.min.js',
                        'src/js/dependencies-larger.min.js',
                        'src/js/app-larger.min.js'
                    ]
                }
            }
        },

        imagemin: {
            all: {
                files: [{
                    expand: true,
                    cwd: 'src/img/',
                    src: ['**/*.{png,jpeg,jpg,gif}'],
                    dest: 'dist/img/'
                }],
                options: {
                    cache: false
                }
            }
        },

        svgmin: {
            options: {
                plugins: [
                  { removeViewBox: false },
                  { removeUselessStrokeAndFill: false }
                ]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'src/img/',
                    src: ['**/*.svg'],
                    dest: 'dist/img/'
                }]
            }
        },

        svg2png: {
            all: {
                files: [{
                    src: ['src/img/*.svg'],
                    dest: ''
                }]
            }
        },

        ftpush: {
            production: {
                auth: {
                    host: 'meinhotspot.com',
                    port: 21,
                    authKey: 'key2'
                },
                src: 'dist',
                dest: '/marketing--staging',
                exclusions: ['**/.*', '**/Thumbs.db', 'node_modules'],
                // keep: ['/important/images/at/img/*.jpg'],
                simple: true,
                useList: false
            }
        },

        watch: {
            html: {
                files: ['src/**/*.mustache', 'src/**/*.json'],
                tasks: ['buildhtml'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: ['src/less/**/*.less'],
                tasks: ['buildcss'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: ['src/js/**/*.js','!src/js/**/*.min.js'],
                tasks: ['buildjs'],
                options: {
                    livereload: true
                }
            },
            imgraster: {
                files: ['src/img/**/*.{jpg,jpeg,gif,png}'],
                tasks: ['buildimagesraster']
            },
            imgvector: {
                files: ['src/img/**/*.svg'],
                tasks: ['buildimagesvector']
            }
        }

    } );

    grunt.registerTask( 'default', ['build'] );

    grunt.registerTask( 'buildhtml',  ['mustache_render:all', 'prettify:all'] );
    grunt.registerTask( 'buildcss',  ['less', 'autoprefixer'] );
    grunt.registerTask( 'buildmodernizr', ['modernizr'] );
    grunt.registerTask( 'buildjs',  ['uglify'] );
    grunt.registerTask( 'buildimagesraster',  ['imagemin'] );
    grunt.registerTask( 'buildimagesvector',  ['svgmin', 'svg2png'] );
    grunt.registerTask( 'buildimages',  ['buildimagesraster', 'buildimagesvector'] );

    grunt.registerTask( 'deploy_production',  ['ftpush:production'] );

    grunt.registerTask( 'build',  ['buildhtml', 'buildcss', 'buildmodernizr', 'buildjs', 'buildimages'] );
};
