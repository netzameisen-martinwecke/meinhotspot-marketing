<?php
	error_reporting(E_ERROR | E_WARNING | E_PARSE);

	require_once( 'MeinHotspotAPI.php' );

	$API = new meinHotspotAPI();

	// get customer
	if( $_GET['action'] === 'getCustomer' && intval( $_GET['customerId'] ) ) {
		$customerId = intval( $_GET['customerId'] );
		$results = $API->getCustomer( $customerId );

		if( $results ) {
			print_r( json_encode( $results ) );
		}
	}

	// get location
	if( $_GET['action'] === 'getLocation' && intval( $_GET['customerId'] ) && intval( $_GET['locationId'] ) ) {
		$customerId = intval( $_GET['customerId'] );
		$locationId = intval( $_GET['locationId'] );

		$results = $API->getLocation( $customerId, $locationId );
		if( $results ) {
			print_r( json_encode( $results ) );
		}
	}

	// put feedback
	if( $_POST['action'] === 'putFeedback' && intval( $_POST['locationId'] ) && intval( $_POST['rate'] ) ) {
		$locationId = intval( $_POST['locationId'] );
		$comment = $_POST['comment'];
		$rate = intval( $_POST['rate'] );
		$email = $_POST['email'];

		$results = $API->putFeedback( $locationId, $comment, $rate, $email );
		if( $results ) {
			print_r( json_encode( $results ) );
		}
	}
