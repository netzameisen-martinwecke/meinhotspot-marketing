<?php

require_once( 'restclient.php' );

class MeinHotspotAPI {
    protected $settings;
    protected $API;

    public function __construct() {
    	$this->settings = array(
    		'url_base' =>		 'http://api.meinhotspot.com',
    		'url_auth' =>		 '/oauth/access_token',
    		'url_customer' =>	 '/v1/customer/{customerId}',
    		'url_location' =>	 '/v1/customer/{customerId}/location/{locationId}',
    		'url_feedback' => 	 '/v1/feedback',
    		'user_id' =>		 '20',
    		'user_name' =>		 'martin.wecke@netzameisen.com',
    		'password' =>		 'AhUaIKjUjEWqA9sL',
    		'client_id' =>		 'FAocuzNmhJ9weJI',
    		'client_secret' =>	 'zHnrMbpZtu8imfAuRTb2EZUfCSP3Di'
    	);

    	$this->setupAPI();
    }


	/**
	 * Setup API
	 * @return [type] [description]
	 */
	public function setupAPI() {
		$auth_token = $this->getAccessToken();

		// exit if access token could not retrieved
		if( !$auth_token->access_token ) {
			return false;
		}

		$this->API = new RestClient(
			array(
				'base_url' => $this->settings['url_base'],
				'headers' => array(
					'Authorization' => 'Bearer ' . $auth_token->access_token
				)
			)
		);
	}


	public function getResults( $url, $param = array() ) {
		$results = $this->API->get(
			$url,
			$param
		);

		if( $results->info->http_code != 200 ) {
			return false;
		}

		return $results->decode_response();
	}


	public function getCustomer( $customerId = 0 ) {
		if( !$this->API ) {
			return false;
		}

		if( !$customerId ) {
			return false;
		}

		$url = str_replace( '{customerId}', $customerId, $this->settings['url_customer'] );
		return $this->getResults( $url );
	}


	public function getLocation( $customerId = 0, $locationId = 0 ) {
		if( !$this->API ) {
			return false;
		}

		if( !$customerId || !$locationId ) {
			return false;
		}

		$url = str_replace( array( '{customerId}', '{locationId}' ), array( $customerId, $locationId ), $this->settings['url_location'] );

		return $this->getResults( $url );
	}


	public function putFeedback( $locationId = 0, $comment = '', $rate = 1, $email = '' ) {
		if( !$this->API ) {
			return false;
		}

		if( !$locationId ) {
			return false;
		}

		$data = array(
			'locationId' 	=> $locationId,
			'comment' 		=> $comment,
			'rate' 			=> $rate,
			'email' 		=> $email
		);

		$results = $this->API->put(
			$this->settings['url_feedback'],
			json_encode( $data ),
			array(
				'Content-Type' => 'application/json'
			)
		);

		if( $results->info->http_code != 200 ) {
			return false;
		}

		return $results->decode_response();
	}


	/**
	 * Get auth token
	 * http://api.meinhotspot.dev3.secu-ring.de/documentation/oauth.md#authenticate-to-the-api
	 * @return Object Auth response
	 */
	public function getAccessToken() {
		if( !$this->settings['url_base']
		 || !$this->settings['url_auth']
		 || !$this->settings['user_id']
		 || !$this->settings['user_name']
		 || !$this->settings['password']
		 || !$this->settings['client_id']
	     || !$this->settings['client_secret'] ) {
			return false;
		}

		$connection = new \RestClient(
			array(
				'base_url' 	=> $this->settings['url_base']
			)
		);

		$connection->set_option(
			'parameters', array(
				'client_id' 		=> $this->settings['client_id'],
				'client_secret' 	=> $this->settings['client_secret'],
				'grant_type' 		=> 'password',
				'username' 		    => $this->settings['user_name'],
				'password' 			=> $this->settings['password']
			)
		);

		$result = $connection->post( $this->settings['url_auth'] );

		if( $result->info->http_code == 200 ) {
	    	return $result->decode_response();
	    } else {
	    	return false;
	    }
	}
}

new MeinHotspotAPI();
