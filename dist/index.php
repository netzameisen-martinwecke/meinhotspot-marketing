<!doctype html>
<html class="no-js">
<head>
	<meta charset="utf-8">

	<title>Willkommen bei MeinHotspot</title>

	<script>
document.getElementsByTagName( 'html' )[0].className += ' js';
document.getElementsByTagName( 'html' )[0].className = document.getElementsByTagName( 'html' )[0].className.replace( 'no-js', '' );
	</script>

	<link rel="stylesheet" href="css/style.css">

	<meta name="HandheldFriendly" content="True">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="cleartype" content="on">

    <link rel="icon" href="img/favicon.png" type="image/png">

<?php
	error_reporting( E_ERROR | E_WARNING | E_PARSE );

	$fallbackData = array(
		'customer_id' => 2723,
		'location_id' => 3344,
		// enable/disable the places widget
		'local-info' => true,
		// enable/disable the
		'rating-modul' => true
	);

	$data = array();

	if( $_GET['meta'] ) {
		$data = $_GET['meta'];
		$data = @base64_decode( $data );
		$data = ( array ) @json_decode( $data );
	}

	if( $_GET['debug'] ) {
		$data = $fallbackData;
	}

	if( $data ) {
?>

	<meta name="location" content=""
		data-customer="<?php echo htmlspecialchars( $data['customer_id'] ); ?>"
		data-location="<?php echo htmlspecialchars( $data['location_id'] ); ?>"
<?php
		if( $data['local-info'] ) {
?>
		data-places="1"
<?php
		} else {
?>
		data-places="0"
<?php
		}
?>

<?php
		if( $data['rating-modul'] ) {
?>
		data-feedback="1"
<?php
		} else {
?>
		data-feedback="0"
<?php
		}
?>
>

<?php
		}
?>

	<script src="js/all-global.min.js"></script>

</head>

    <body>

<section class="head">
	<div class="shell">
		<h2 class="head-title" data-title-role="title">
			Kundenname
		</h2>
	</div>
</section>

        <section class="content" role="main" id="content">
            <div class="shell">

                <div class="widgets">

<div class="widget widget--banner">
	<div class="widget-content widget-content--banner">
		<img src="img/dummy.jpg" class="banner-image">
	</div>
</div>

<div class="widget widget--facebook has-icon" data-collapsable-role="widget">
	<h3 class="widget-title widget-title--facebook" data-collapsable-role="toggle">
			Folgen Sie uns bei Facebook
	</h3>

	<div class="widget-content widget-content--facebook">
		<div class="facebook-button">

			<div class="fb-like"
				data-href="https://facebook.com/meinhotspot"
				data-layout="button_count"
				data-action="like"
				data-size="large"
				data-show-faces="false"
				data-share="false">
			</div>

			<script>
(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.7";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
			</script>

		</div>

	</div>
</div>

<div class="widget widget--magazines fullwidth has-icon" data-collapsable-role="widget">
	<h3 class="widget-title widget-title--magazines" data-collapsable-role="toggle">
		Zeitschriften lesen
	</h3>

	<div class="widget-content widget-content--magazines">
		<div class="magazines-info">
			<p>
				An diesem Standort können Sie kostenlos in über 100 aktuellen Tageszeitungen und Magazinen lesen.
			</p>
		</div>

		<div class="magazines-button">
			<a href="https://appurl.io/j5tkr657/" class="magazines-button-button button">
				zu den Zeitschriften
			</a>
		</div>
	</div>
</div>

<div class="widget widget--feedback has-icon" data-collapsable-role="widget">
    <h3 class="widget-title widget-title--feedback" data-collapsable-role="toggle">
        Bitte bewerten Sie uns
    </h3>

    <div class="widget-content widget-content--feedback">

    </div>
</div>


<script id="template--feedback" type="x-tmpl-mustache">

    <p class="feedback-intro">
        Wie gefällt es Ihnen bei uns? Wir würden uns über eine Bewertung und einen kurzen Kommentar freuen.
    </p>

    <div class="feedback-form">

        <div class="feedback-label">
            Ihre Bewertung:
        </div>

        <div class="feedback-rate">
            <input type="radio" class="feedback-rate-input" name="feedback-rate" value="5" id="feedback-rate--5">
            <label class="feedback-rate-label" for="feedback-rate--5">5</label>
            <input type="radio" class="feedback-rate-input" name="feedback-rate" value="4" id="feedback-rate--4">
            <label class="feedback-rate-label" for="feedback-rate--4">4</label>
            <input type="radio" class="feedback-rate-input" name="feedback-rate" value="3" id="feedback-rate--3">
            <label class="feedback-rate-label" for="feedback-rate--3">3</label>
            <input type="radio" class="feedback-rate-input" name="feedback-rate" value="2" id="feedback-rate--2">
            <label class="feedback-rate-label" for="feedback-rate--2">2</label>
            <input type="radio" class="feedback-rate-input" name="feedback-rate" value="1" id="feedback-rate--1">
            <label class="feedback-rate-label" for="feedback-rate--1">1</label>
        </div>

        <div class="feedback-comment">
            <div class="form-item fancylabels">
                <label class="form-label form-label--text" for="feedback-comment">
                    Ihr Kommentar ...
                </label>

                <textarea type="text" class="feedback-comment-input form-input form-input--text" name="feedback-comment" id="feedback-comment" data-feedback-role="comment-input"></textarea>
            </div>

            <div class="form-item fancylabels">
                <label class="form-label form-label--text" for="feedback-email">
                    Ihre E-Mail-Adresse (nur für Rückfragen)
                </label>
                <input type="email" class="feedback-email-input form-input form-input--text" name="feedback-email" id="feedback-email" data-feedback-role="email-input"></textarea>
            </div>

            <a class="feedback-submit form-input form-input--submit button">
                Absenden
            </a>
        </div>

    </div>

    <div class="feedback-message feedback-message--success">
        Vielen Dank für Ihre Bewertung.
    </div>

</script>

<div class="widget widget--newsletter has-icon" data-collapsable-role="widget">
	<h3 class="widget-title widget-title--newsletter" data-collapsable-role="toggle">
		Newsletter abonnieren
	</h3>

	<div class="widget-content widget-content--newsletter">
		<div class="newsletter-info">
			<p>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
			</p>
		</div>

		<div class="newsletter-form">
			<div class="form-item form-item--text fancylabels">
				<label class="form-label form-label--text" for="email">
					Ihre E-Mail-Adresse
				</label>

				<input name="email" id="email" class="newsletter-form-input form-input form-input--text" autocomplete="off" value="" type="text">

				<input name="subscribe" id="subscribe" class="newsletter-form-submit form-input form-input--submit" value="Abonnieren" type="submit">
			</div>
		</div>

	</div>
</div>

                </div>

<div class="go">

	<div class="go-skip">
		<input type="checkbox" id="skipnexttime" class="go-skip-input">
		<label class="go-skip-label" for="skipnexttime">
			Diese Seite nicht mehr anzeigen
		</label>
	</div>

	<a href="#" class="go-button button">
		Weiter
	</a>
</div>

            </div>
        </section><!-- /.content -->

    </body>
</html>
